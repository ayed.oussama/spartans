/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.entities;

/**
 *
 * @author Dagdoug
 */
public class Categorie {
    private int id;
    private int nombrePoints;
    private Admin admin;
    private Offre[] offres;

    public Categorie() {
    }

    public Categorie(int nombrePoints, Admin admin, Offre[] offres) {
        this.nombrePoints = nombrePoints;
        this.admin = admin;
        this.offres = offres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public int getNombrePoints() {
        return nombrePoints;
    }

    public void setNombrePoints(int nombrePoints) {
        this.nombrePoints = nombrePoints;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Offre[] getOffres() {
        return offres;
    }

    public void setOffres(Offre[] offres) {
        this.offres = offres;
    }

}
