/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.A4D.dao.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pi.A4D.dao.interfaces.IcrudDAO;
import pi.A4D.entities.Panier;
import pi.A4D.outils.DataConnection;

/**
 *
 * @author Ayed
 */
public class PanierDAO implements IcrudDAO<Panier>{
     private Connection connection;

    public PanierDAO() {
        connection = DataConnection.getInstance().getConnection();
    }

    @Override
    public void add(Panier t) {
 String sql = "INSERT INTO panier (qte,id_produit, id_membre) VALUES (?,?,?)";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setInt(1, t.getQte());
                preparedStatement.setInt(2, t.getProduit().getIdOffre());
                preparedStatement.setInt(3,t.getMembre().getId());
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "insert failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
    }

    @Override
    public void update(Panier t) {
      
    }

    @Override
    public void delete(int id) {
String sql = "delete from panier where id = ?";
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement(sql);
       
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate(); 
                System.out.println(sql);
            } catch (SQLException ex) {
                Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "insert failed", ex);
            } finally {
                
                try {
                    if (preparedStatement != null) {
                        preparedStatement.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
                }
            }
    }

    @Override
    public List<Panier> displayAll() {
          List<Panier> list = new ArrayList<Panier>();
        String sql = "SELECT * FROM panier";
        Statement statement = null; 
        ResultSet resultSet = null; 
        
        try {
            statement = connection.createStatement(); 
            resultSet = statement.executeQuery(sql);  
            System.out.println(sql);
            while (resultSet.next()) {
                Panier panier = new Panier();
                panier.setId(resultSet.getInt("id"));
                panier.setQte(resultSet.getInt("qte"));
                //panier.setProduit();
               
                               list.add(panier);
            }


        } catch (SQLException ex) {
            Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "find all employees failed", ex);
        } finally {
            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return list;
    }

    @Override
    public Panier findById(int id) {
      String sql = "SELECT * FROM panier WHERE id = ?"; 
        Panier found = null; 
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery(); 
            System.out.println(sql);
            if (resultSet.next()) {
               found = new Panier();
                found.setId(resultSet.getInt("id"));
                found.setQte(resultSet.getInt("qte"));
                
            }


        } catch (SQLException ex) {
            Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "find panier failed", ex);
        } finally {

            
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, "free resourses failed", ex);
            }
        }
        return found;
    }
   
}
